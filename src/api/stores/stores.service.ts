import { faker } from '@faker-js/faker';
import { IStore } from './store.interface';
import * as db from '../../db';

export const getAll = async (): Promise<IStore[]> => {
  /* Imagine that here we call to the database using something like:
    const data = await db.collection('stores).find(...)
  */

  const data = [
    {
      id: faker.datatype.number(),
      name: faker.lorem.word(),
      address: faker.address.streetAddress(),
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime(),
    },
    {
      id: faker.datatype.number(),
      name: faker.lorem.word(),
      address: faker.address.streetAddress(),
      createdAt: faker.datatype.datetime(),
      updatedAt: faker.datatype.datetime(),
    },
  ];

  return data;
};
