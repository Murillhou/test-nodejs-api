import 'dotenv/config';
import cors from 'cors';
import express, { json, urlencoded } from 'express';
import * as db from './db';
import { userRoutes } from './api/users/users.router';
import { storeRoutes } from './api/stores/stores.router';

const env = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 8080;

const app = express();

app.set('port', port);
app.use(json());
app.use(cors());
app.use(urlencoded({ extended: false }));

app.use('/users', userRoutes);
app.use('/stores', storeRoutes);

const main = async () => {
  await db.connect();
  app.listen(app.get('port'), () =>
    console.log(
      `Test NodeJS API running in port ${app.get('port')} | env: ${env}`
    )
  );
};

main();
