import { Router } from 'express';
import authentication from '../middlewares/auth.middleware';
import { login, getProfile, updateProfile } from './users.controller';

export const userRoutes = Router();

userRoutes.route('/login').post(login);

userRoutes.use(authentication);

userRoutes.route('/profile').get(getProfile).patch(updateProfile);
