import { NextFunction, Request, Response } from 'express';
import { IStore } from './store.interface';
import * as service from './stores.service';

export const getStores = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const stores: IStore[] = await service.getAll();

    return res.json(stores);
  } catch (e: any) {
    return res.status(500).json({ status: 500, message: e.message });
  }
};
