import { NextFunction, Request, Response } from 'express';
import { IUser } from './user.interface';
import * as service from './users.service';

export const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).json({ status: 400, error: 'BAD REQUEST' });
    }

    /* Imagine that here is all the necessary to log in a user
      ...
     */
    const loggedIn = true;

    if (!loggedIn) {
      return res.status(401).json({ status: 401, message: 'UNAUTHORIZED' });
    }

    return res.status(200).json({ email, accessToken: '' });
  } catch (e: any) {
    return res.status(500).json({ status: 500, message: e.message });
  }
};

export const getProfile = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { userId } = req.body;

    if (!userId) {
      return res.status(400).json({ status: 400, error: 'BAD REQUEST' });
    }

    const user: IUser = await service.get(userId);

    return res.status(200).json({ id: user.id, name: user.name, email: user.email });
  } catch (e: any) {
    return res.status(500).json({ status: 500, message: e.message });
  }
};

export const updateProfile = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { userId, name, email } = req.body;

    if (!userId || !name || !email) {
      return res.status(400).json({ status: 400, error: 'BAD REQUEST' });
    }

    const user: IUser = await service.update(userId, { name, email });

    return res.status(200).json({ id: user.id, name: user.name, email: user.email });
  } catch (e: any) {
    return res.status(500).json({ status: 500, message: e.message });
  }
};
