import { Router } from 'express';
import authentication from '../middlewares/auth.middleware';
import { getStores } from './stores.controller';

export const storeRoutes = Router();

storeRoutes.use(authentication);

storeRoutes.route('/').get(getStores);
